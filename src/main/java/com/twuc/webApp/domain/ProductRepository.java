package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    List<Product> findAllByProductLineTextDescriptionContains(String content);
    List<Product> findAllByQuantityInStockBetween(short low, short high);
    List<Product> findAllByQuantityInStockBetweenOrderByProductCode(short low, short high);
    Page<Product> findAllByQuantityInStockBetweenOrderByProductCode(short low, short high, Pageable pageRequest);
    // --end-->
}